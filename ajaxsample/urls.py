from django.conf.urls import patterns, include, url


from dajaxice.core import dajaxice_autodiscover
dajaxice_autodiscover()


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.views.generic import TemplateView



urlpatterns = patterns('',

	(r'^dajaxice/', include('dajaxice.urls')),
	url(r'^$', TemplateView.as_view(template_name="index.html"),name='home'),




	


    # Examples:
    # url(r'^$', 'ajaxsample.views.home', name='home'),
    # url(r'^ajaxsample/', include('ajaxsample.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
